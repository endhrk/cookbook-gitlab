#
# Cookbook Name:: gitlab
# Recipe:: nfs_client
#

gitlab = node['gitlab']
nfs = node['gitlab']['nfs']

## Create mount point
directory nfs['mount_path'] do
  owner gitlab['user']
  group gitlab['group']
  mode 0750
  not_if { File.exist?(nfs['mount_path']) }
end

## Mount
mount nfs['mount_path'] do
  device nfs['device']
  fstype "nfs"
  options "rw"
  action [:mount, :enable]
end

